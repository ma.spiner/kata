# Kata


### US 1:
In order to save money
As a bank client
I want to make a deposit in my account



### US 2:
In order to retrieve some or all of my savings
As a bank client
I want to make a withdrawal from my account



### US 3:
In order to check my operations
As a bank client
I want to see the history (operation, date, amount, balance) of my operations

<br/>
Clone project : git clone https://gitlab.com/ma.spiner/kata.git

### Run project
In project directory : mvn spring-boot:run

Build Docker image : mvn spring-boot:build-image

Run docker image : docker run kata-bank:1.0-SNAPSHOT


API Documentation with Swagger : http://localhost:8080/swagger-ui.html
### cURL commands for the API :

#### Deposit request :
curl --location --request POST 'localhost:8080/operations'
--header 'Content-Type: application/json'
--data-raw '{
"accountNumber" : 1,
"value": 50
}'

#### Withdrawal request :
curl --location --request POST 'localhost:8080/operations'
--header 'Content-Type: application/json'
--data-raw '{
"accountNumber" : 1,
"value": -50
}'

#### GetHistory request :
curl --location --request GET 'localhost:8080/operations?accountNumber=1'