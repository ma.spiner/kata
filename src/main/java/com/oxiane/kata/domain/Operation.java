package com.oxiane.kata.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operation_id")
    private Integer operationId;
    @Column(name = "operation_type")
    private String operationType;
    @Column(name = "operation_value")
    private Double operationValue;
    @Column(name = "operation_date")
    private LocalDateTime operationDate;
    @Column(name = "account_balance")
    private Double accountBalance;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    public Operation(String operationType, Double operationValue, LocalDateTime operationDate,Double accountBalance, Account account){
        this.operationType = operationType;
        this.operationValue = operationValue;
        this.operationDate = operationDate;
        this.accountBalance = accountBalance;
        this.account = account;
    }
}
