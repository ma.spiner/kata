package com.oxiane.kata.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OperationRequest {
    private Long accountNumber;
    private Double value;

    public OperationRequest(Long accountNumber, Double value) {
        this.accountNumber = accountNumber;
        this.value = value;
    }


}
