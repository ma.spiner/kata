package com.oxiane.kata.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long accountId;
    @Column(name = "account_number", unique = true, nullable = false)
    private long accountNumber;
    @Column(name = "account_position")
    private Double accountPosition;

    public Account(Integer accountNumber, Double accountPosition){
        this.accountNumber = accountNumber;
        this.accountPosition = accountPosition;
    }
}
