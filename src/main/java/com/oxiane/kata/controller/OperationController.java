package com.oxiane.kata.controller;

import com.oxiane.kata.domain.Operation;
import com.oxiane.kata.domain.OperationRequest;
import com.oxiane.kata.exception.AccountNotFoundException;
import com.oxiane.kata.exception.NotEnoughFundException;
import com.oxiane.kata.service.OperationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/operations")
public class OperationController {

    private final OperationService operationService;

    public OperationController(OperationService operationService) {
        this.operationService = operationService;
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Operation> performOperation(@RequestBody OperationRequest operationRequest) throws AccountNotFoundException, NotEnoughFundException {
        Operation operation = operationService.performOperation(operationRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(operation);
    }

    @GetMapping
    public ResponseEntity<List<Operation>> getHistory(@RequestParam(name = "accountNumber")Long accountNumber) throws AccountNotFoundException{
        List<Operation> operations = operationService.getHistory(accountNumber);
        return ResponseEntity.status(HttpStatus.OK).body(operations);
    }
}