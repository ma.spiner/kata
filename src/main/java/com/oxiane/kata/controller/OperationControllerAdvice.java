package com.oxiane.kata.controller;

import com.oxiane.kata.exception.NotEnoughFundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class OperationControllerAdvice {


    @ExceptionHandler(NotEnoughFundException.class)
    @ResponseBody
    public ResponseEntity<String> notEnoughFundHandler(NotEnoughFundException notEnoughFundException){
        return new ResponseEntity<>(notEnoughFundException.getMessage(),HttpStatus.FORBIDDEN);
    }
}
