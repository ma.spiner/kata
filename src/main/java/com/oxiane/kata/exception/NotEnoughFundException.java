package com.oxiane.kata.exception;

public class NotEnoughFundException extends Exception{
    public NotEnoughFundException (long accountNumber){
        super("Not enough fund on account: "+accountNumber);
    }
}
