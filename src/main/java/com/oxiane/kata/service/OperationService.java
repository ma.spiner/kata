package com.oxiane.kata.service;

import com.oxiane.kata.domain.Operation;
import com.oxiane.kata.domain.OperationRequest;
import com.oxiane.kata.exception.AccountNotFoundException;
import com.oxiane.kata.exception.NotEnoughFundException;

import java.util.List;

public interface OperationService {
    Operation performOperation(OperationRequest operationRequest) throws AccountNotFoundException, NotEnoughFundException;
    List<Operation> getHistory(Long accountNumber) throws AccountNotFoundException;
}
