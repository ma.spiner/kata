package com.oxiane.kata.service;

import com.oxiane.kata.domain.Account;
import com.oxiane.kata.exception.AccountNotFoundException;

public interface AccountService {
    Account getAccount(long accountId) throws AccountNotFoundException;

}
