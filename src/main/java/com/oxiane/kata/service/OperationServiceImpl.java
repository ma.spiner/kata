package com.oxiane.kata.service;

import com.oxiane.kata.domain.Account;
import com.oxiane.kata.domain.Operation;
import com.oxiane.kata.domain.OperationRepository;
import com.oxiane.kata.domain.OperationRequest;
import com.oxiane.kata.exception.AccountNotFoundException;
import com.oxiane.kata.exception.NotEnoughFundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OperationServiceImpl implements OperationService{

    private AccountService accountService;
    private OperationRepository operationRepository;

    public OperationServiceImpl(AccountService accountService,OperationRepository operationRepository){
        this.accountService = accountService;
        this.operationRepository = operationRepository;
    }


    @Override
    public Operation performOperation(OperationRequest operationRequest) throws AccountNotFoundException, NotEnoughFundException {
        Account account = accountService.getAccount(operationRequest.getAccountNumber());
        double balance = account.getAccountPosition() + operationRequest.getValue();
        String operationType = "";
        if(operationRequest.getValue ()>=0){
            operationType = "deposit";
        } else {
            operationType = "withdrawal";
            if(balance<0){
                throw new NotEnoughFundException (operationRequest.getAccountNumber ());
            }
        }

        Operation operation = new Operation(operationType,
                operationRequest.getValue(),
                LocalDateTime.now(),
                balance,
                account);

        account.setAccountPosition(account.getAccountPosition()+operation.getOperationValue());
        return operationRepository.save(operation);
    }

    @Override
    public List<Operation> getHistory(Long accountNumber) throws AccountNotFoundException {
        Account account = accountService.getAccount (accountNumber);
        List<Operation> operations = operationRepository.findByAccount(account);
        return operations;
    }
}
