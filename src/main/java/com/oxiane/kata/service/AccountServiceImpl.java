package com.oxiane.kata.service;

import com.oxiane.kata.domain.Account;
import com.oxiane.kata.domain.AccountRepository;
import com.oxiane.kata.exception.AccountNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getAccount (long accountNumber) throws AccountNotFoundException {
        return accountRepository.findByAccountNumber(accountNumber)
                .orElseThrow (() -> new AccountNotFoundException (accountNumber));
    }
}
