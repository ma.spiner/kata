INSERT INTO Account (account_number, account_position)
VALUES (1,150);

INSERT INTO Operation (operation_type, operation_value, operation_date, account_balance, account_id)
VALUES ('deposit',10,parsedatetime('13-04-2021 12:00:00.000', 'dd-MM-yyyy hh:mm:ss.SS'),150,
(select account_id from Account where account_number=1)
);