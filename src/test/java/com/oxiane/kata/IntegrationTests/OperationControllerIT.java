package com.oxiane.kata.IntegrationTests;

import com.oxiane.kata.controller.OperationController;
import com.oxiane.kata.service.OperationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class OperationControllerIT {

    private OperationController operationController;

    private MockMvc mockMvc;

    @Mock
    OperationService operationService;

    private String url = "/operations";

    @BeforeEach
    public void setup(){
        this.operationController = new OperationController(operationService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(operationController).build();
    }

    @Test
    public void deposit() throws Exception {
        String request = "{\"accountNumber\":1,\"operationType\":\"deposit\",\"value\":50}";

        mockMvc.perform(post(url)
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void withdrawal() throws Exception {
        String request = "{\"accountNumber\":1,\"operationType\":\"deposit\",\"value\":-50}";

        mockMvc.perform(post(url)
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void getHistory() throws Exception {

        mockMvc.perform(get(url+"?accountNumber=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
