package com.oxiane.kata.IntegrationTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EndToEndOperationTest {

    @Autowired
    MockMvc mockMvc;

    private static final String url = "/operations/";

    /*
    US 1:
        In order to save money
        As a bank client
        I want to make a deposit in my account
     */

    @Test
    public void should_register_a_deposit () throws Exception {

        String request_body = "{\"accountNumber\":\"1\",\"value\":50}";

        this.mockMvc.perform (post (url)
                .content (request_body)
                .contentType (MediaType.APPLICATION_JSON))
                .andExpect (status ().isCreated ());
    }

        /*
    US 2:
        In order to retrieve some or all of my savings
        As a bank client
        I want to make a withdrawal from my account
     */

    @Test
    public void should_register_a_withdrawal() throws Exception {

        String request_body = "{\"accountNumber\":\"1\",\"value\":-50}";

        this.mockMvc.perform(post(url)
                .content(request_body)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
    @Test
    public void should_get_not_enough_fund() throws Exception {

        String request_body = "{\"accountNumber\":\"1\",\"value\":-5000}";

        this.mockMvc.perform(post(url)
                .content(request_body)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

        /*
    US 3:
        In order to check my operations
        As a bank client
        I want to see the history (operation, date, amount, balance) of my operations
     */

    @Test
    public void should_get_operation_history() throws Exception {

        this.mockMvc.perform(get(url+"?accountNumber=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].operationType").value("deposit"))
                .andExpect(jsonPath("[0].operationValue").value(10));
    }
}
