package com.oxiane.kata.UnitTests;

import com.oxiane.kata.domain.Account;
import com.oxiane.kata.domain.Operation;
import com.oxiane.kata.domain.OperationRepository;
import com.oxiane.kata.domain.OperationRequest;
import com.oxiane.kata.exception.AccountNotFoundException;
import com.oxiane.kata.exception.NotEnoughFundException;
import com.oxiane.kata.service.AccountService;
import com.oxiane.kata.service.OperationService;
import com.oxiane.kata.service.OperationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OperationServiceTest {
    private OperationService operationService;

    private Account account;

    @Mock
    private OperationRepository operationRepository;

    @Mock
    private AccountService accountService;

    @BeforeEach
    public void setup(){
        this.operationService = new OperationServiceImpl(accountService,operationRepository);
        this.account = new Account(1,0d);
    }

    @Test
    public void depositTest() throws AccountNotFoundException, NotEnoughFundException {
        OperationRequest operationRequest = new OperationRequest(1l,50d);
        Operation operationExpected = new Operation("deposit",50d, LocalDateTime.now(),50d,this.account);

        when(operationRepository.save(any()))
                .thenReturn(operationExpected);
        when(accountService.getAccount(anyLong()))
                .thenReturn(this.account);

        Operation operationReturned = operationService.performOperation(operationRequest);
        assertThat(operationReturned,is(operationExpected));
    }

    @Test
    public void withdrawalTest() throws AccountNotFoundException, NotEnoughFundException {
        account.setAccountPosition(500d);
        OperationRequest operationRequest = new OperationRequest(1l,-50d);
        Operation operationResponseExpected = new Operation("withdrawal",-50d, LocalDateTime.now(),-50d,account);

        when(operationRepository.save(any()))
                .thenReturn(operationResponseExpected);
        when(accountService.getAccount (anyLong()))
                .thenReturn(this.account);

        Operation operationReturned = operationService.performOperation(operationRequest);
        assertThat(operationReturned,is(operationResponseExpected));
    }


    @Test
    public void withdrawalWithoutEnoughFounds() throws AccountNotFoundException, NotEnoughFundException {

        assertThrows(NotEnoughFundException.class,()->{
            when(accountService.getAccount (anyLong()))
                    .thenReturn(this.account);
            account.setAccountPosition(0d);
            OperationRequest operationRequest = new OperationRequest(1l,-50d);
            Operation operationReturned =  operationService.performOperation (operationRequest);
        });
    }
    @Test
    public void getHistoryTest()throws AccountNotFoundException{
        Operation operationTest1 = new Operation("deposit",50d, LocalDateTime.now(),50d,account);
        Operation operationTest2 = new Operation("withdrawal",-50d, LocalDateTime.now(),0d,account);
        List<Operation> operationsExpected = Arrays.asList(operationTest1,operationTest2);
        when(operationRepository.findByAccount(any()))
                .thenReturn(operationsExpected);

        assertThat(operationService.getHistory(1l),is(operationsExpected));

    }

}
