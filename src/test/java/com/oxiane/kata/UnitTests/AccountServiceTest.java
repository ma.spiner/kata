package com.oxiane.kata.UnitTests;

import com.oxiane.kata.domain.Account;
import com.oxiane.kata.domain.AccountRepository;
import com.oxiane.kata.exception.AccountNotFoundException;
import com.oxiane.kata.service.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    private AccountServiceImpl accountService;

    @BeforeEach
    public void setup(){
        this.accountService = new AccountServiceImpl(accountRepository);
    }

    @Test
    public void getAccountTest() throws AccountNotFoundException {
        Account expectedAccount = new Account(1,0d);
        when(accountRepository.findByAccountNumber(anyLong()))
                .thenReturn(Optional.of(expectedAccount));

        Account account = accountService.getAccount(1);
        assertThat(account,is(expectedAccount));
    }

    @Test
    public void getAccountFromAccountNumberNotFoundTest() throws AccountNotFoundException{
        assertThrows(AccountNotFoundException.class,()->{
            when(accountRepository.findByAccountNumber(anyLong()))
                    .thenReturn(Optional.empty());
            accountService.getAccount(000);
        });
    }
}
